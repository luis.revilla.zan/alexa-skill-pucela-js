# Alexa Skill Server - Pucela JS

## To make [POST] /alexa endpoint available

### 1. Install the dependencies
<code>npm install</code>

### 2. Run the server
<code>npm start</code>

### 3. Use ngrok to allow an https connection
<code>npm run serve-https</code>