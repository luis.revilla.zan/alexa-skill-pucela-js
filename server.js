const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const bootstrap = () => {
    app.use(bodyParser.json());
    app.listen(4500);

    // Endpoint
    app.post('/alexa', async (req, res, next) => {
        console.log('You hit [POST] /alexa endpoint');
        return res.status(200).send({});
    });
};


bootstrap();